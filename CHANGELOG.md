# Change Log
All notable changes to this project will be documented in this file.

## Release 0.2.2
- Allowed optional external management of deprecated PS1 storage folder

## Release 0.2.1
- Consolidated PS1 calls to a single Exec statement
- Removed helpers_dir parameter
- Refactored unit tests
- Updated Helpers to 2024.07.29

## Release 0.2.0
- Updated Helpers to 2024.06.27

## Release 0.1.9
- Updated to PDK 3.0.0
- Updated examples
- Shifted from puppetlabs/powershell 5.0.0 to 5.2.0
- Cleaned up linting and unit tests
- Converted .ERB to .EPP templates
- Refactored PS1-based code
- Dropped Server 2012 R2

## Release 0.1.8
- Skipped

## Release 0.1.7
- Added Server 2022 as supported OS
- Updated to PDK 2.4.0
- Refactor to support new PowerShell helpers

## Release 0.1.6
- Adjusted method used to read log_output variable, and update upper bounds for dependencies

## Release 0.1.5
- Refactored (to align better with file_lnk)

## Release 0.1.4
- Skipped

## Release 0.1.3
- Skipped

## Release 0.1.2
- Initial release