require 'spec_helper'

describe 'file_url' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      context 'with all parameters' do
        let(:title) { 'url_shortcut_test01' }
        let(:params) do
          {
            file_name: 'ShortcutURL01.url',
            parent_folder: 'C:\\Users\\Public\\Desktop',
            url: 'http://forge.puppet.com',
            ensure: 'present',
            icon_index: 0,
            icon_file: 'C:\\Windows\\HelpPane.exe',
            log_output: true,
          }
        end

        it { is_expected.to compile }
        it { is_expected.to contain_exec("Set 'ShortcutURL01.url' file_url shortcut") }
        it { is_expected.to contain_file('C:/ProgramData/Puppet/file_url').with('ensure' => 'absent') }
        it { is_expected.to contain_notify("file_url - ShortcutURL01.url - ensure: 'present'") }
        it { is_expected.to contain_notify("file_url - ShortcutURL01.url - icon_file: 'C:\\Windows\\HelpPane.exe'") }
        it { is_expected.to contain_notify("file_url - ShortcutURL01.url - icon_index: '0'") }
        it { is_expected.to contain_notify("file_url - ShortcutURL01.url - parent_folder: 'C:\\Users\\Public\\Desktop'") }
        it { is_expected.to contain_notify("file_url - ShortcutURL01.url - url: 'http://forge.puppet.com'") }
      end

      context 'with missing file_name parameter' do
        let(:title) { 'url_shortcut_test02' }
        let(:params) do
          {
            parent_folder: 'C:\\Users\\Public\\Desktop',
            url: 'http://forge.puppet.com',
          }
        end

        it { is_expected.to compile.and_raise_error(%r{expects a value for parameter 'file_name'}) }
      end

      context 'with ensure absent' do
        let(:title) { 'url_shortcut_test03' }
        let(:params) do
          {
            file_name: 'ShortcutURL03.url',
            parent_folder: 'C:\\Users\\Public\\Desktop',
            ensure: 'absent',
          }
        end

        it { is_expected.to compile }
        it { is_expected.to contain_tidy("Remove 'ShortcutURL03.url' file_url shortcut") }
      end
    end
  end
end
