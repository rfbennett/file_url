# @summary File URL
#
# Defines a .URL shortcut
#
# @param ensure
#   Desired state of the shortcut
#   Example: 'absent'
#
# @param file_name
#   Assigned text, including the file extension, that should be used as the name of the file
#      Ensure that the exact file name is used if 'ensure' is set to 'absent'
#   Example: 'ThanksForAllTheFish.url'
#
# @param hot_key
#   Denotes the key combination to assign to the shortcut
#   Key combo execution may not work when files are in areas that current user does not have direct rights to
#   Example: 1607
#
# @param icon_file
#   Full path to the file containing the icon to use for the shortcut
#   Example: 'C:\\Windows\\HelpPane.exe'
#
# @param icon_index
#   Denotes which entry within the icon_file file should be used
#   Example: 0
#
# @param log_output
#   Determines additional logging output (typically only used for troubleshooting) during a Puppet agent run
#   Example: True
#
# @param parent_folder
#   Fully qualified path to the parent folder that will house the shortcut
#   Example: 'C:\\Users\\Public\\Desktop'
#
# @param url
#   URL to navigate to when the shortcut is executed
#      Required when 'ensure' is set to 'present'
#   Example: 'https://forge.puppet.com'
#
define file_url (
  String $file_name,
  String $parent_folder,
  Enum['present', 'absent'] $ensure = 'present',
  Optional[String] $hot_key = undef,
  String $icon_file = 'absent',
  Integer $icon_index = -1,
  Boolean $log_output = false,
  Optional[String] $url = undef,
) {
  if ($log_output) {
    notify { "${module_name} - ${file_name} - ensure: '${ensure}'": }
    notify { "${module_name} - ${file_name} - parent_folder: '${parent_folder}'": }
  }
  # Cleanup of unwanted folder structure from prior versions (this will be removed in a subsequent version)
  if ! defined(File["C:/ProgramData/Puppet/${module_name}"]) {
    file { "C:/ProgramData/Puppet/${module_name}" :
      ensure  => absent,
      recurse => true,
      force   => true,
    }
  }
  if ($ensure == 'absent') {
    tidy { "Remove '${file_name}' ${module_name} shortcut":
      path    => $parent_folder,
      age     => '0',
      recurse => 1,
      matches => [$file_name],
    }
  } else {
    if ($log_output) {
      notify { "${module_name} - ${file_name} - icon_file: '${icon_file}'": }
      notify { "${module_name} - ${file_name} - icon_index: '${icon_index}'": }
      notify { "${module_name} - ${file_name} - url: '${url}'": }
    }
    if ($url == undef) or ($url == '') {
      fail "Invalid/Empty 'url' supplied for '${file_name}'"
    }
    $epp_param_hash = {
      'file_name'     => $file_name,
      'hot_key'       => $hot_key,
      'identifier'    => $module_name,
      'icon_file'     => $icon_file,
      'icon_index'    => $icon_index,
      'log_output'    => $log_output,
      'parent_folder' => $parent_folder,
      'url'           => $url,
    }
    exec { "Set '${file_name}' ${module_name} shortcut":
      provider  => powershell,
      command   => "${file("${module_name}/helpers.ps1")};
                    ${epp("${module_name}/command.ps1.epp", $epp_param_hash)}",
      onlyif    => "${file("${module_name}/helpers.ps1")};
                    ${epp("${module_name}/onlyif.ps1.epp", $epp_param_hash)}",
      logoutput => $log_output,
    }
  }
}
