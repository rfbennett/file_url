# File URL

Puppet defined type for managing .URL files within Microsoft Windows.

## Table of Contents

1. [Description](#description)
2. [Usage - Configuration options and additional functionality](#usage)
3. [Limitations - OS compatibility, etc.](#limitations)
4. [Development - Guide for contributing to the module](#development)

## Description

This module supplies a defined type that can be used to manage .URL shortcut files on a Windows node. It leverages PowerShell to do the bulk of the heavy lifting.

## Usage

To use the file_url type, you must (at a minimum) specify file_name, parent_folder and url (as shown below). Note that, where possible, the shortcut creation process will try to automatically generate missing parent folders.

You can make a single shortcut:
```
  file_url { 'JustOneShortcut':
    file_name     => 'SingleShortcutExample.url',
    parent_folder => 'C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\AFolder',
    url           => 'https://forge.puppet.com',
  }
```

Or you can loop through an array (of hashes) of shortcuts and do many at once (note that a Title is not required within the Array as it gets added when supplying the data to the defined resource type):
```
class just_an_example_class (
  Array[Hash] $shortcuts_url = {
    {
      file_name     => 'BasicShortcutExample',
      parent_folder => 'C:\\Users\\Public\\Desktop',
      url           => 'https://www.puppet.com',
    },
    {
      file_name     => 'AnotherSimpleShortcutExample',
      parent_folder => 'C:\\Users\\Public\\Desktop',
      url           => 'https://www.puppet.com',
      log_output    => true,
    },
    {
      file_name     => 'YetAnotherShortcutExample',
      parent_folder => 'C:\\Users\\Public\\Desktop',
      url           => 'https://www.gitlab.com',
      hot_key       => 1607,
      icon_file     => 'C:\\Program Files\\Git\\git-cmd.exe',
      icon_index    => 0,
      log_output    => true,
    },
    {
      file_name     => 'OneMoreShortcutExample',
      parent_folder => 'C:\\Users\\Public\\Desktop',
      url           => 'https://forge.puppet.com',
      hot_key       => 0,
      icon_file     => 'C:\\Folder1\\puppet.ico',
      icon_index    => 0,
      log_output    => true,
    }
  },
) {
  $shortcuts_url.each | String Hash $shortcuts_url_hash | {
    file_url { $shortcuts_url_hash['file_name']:
      * => $shortcuts_url_hash
    }
  }
}
```
You can also remove a shortcut:
```
  file_url { 'ShortcutCleanup':
    ensure        => 'absent',
    file_name     => 'UnwantedShortcut.url',
    parent_folder => 'C:\\Users\\Public\\Desktop',
  }
```

## Limitations

1. This modules uses the following to generate the contents of the REFERENCE.md file:
  * `puppet strings generate --format markdown --out REFERENCE.md`
2. This module has been evaluated against the following:
  * Microsoft Windows Server 2022 (running PowerShell 5.1.20348.643)
  * Microsoft Windows Server 2019 (running PowerShell 5.1.17763.1971)
  * Microsoft Windows Server 2016 (running PowerShell 5.1.14393.4530)
  * Microsoft Windows 10 (running PowerShell 5.1.19041.1023)

## Development

Feedback and ideas are always welcome - please contact an Author (listed in metadata.json) to discuss your input, or feel free to simply [open an Issue](https://gitlab.com/rfbennett/file_url/-/issues).
